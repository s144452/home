```
board |-> "fork"*"fork" |- 
  space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork")
‖ space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork")

⇓

board |-> "fork" |-                                                                  board |-> nil |- 
  space.get("fork"); space.put("fork"); space.put("fork")                   =>       space.get("fork"); space.put("fork"); space.put("fork")
‖ space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork")       ‖ space.get("fork"); space.put("fork"); space.put("fork")

⇓

board |-> nil |- 
  space.put("fork"); space.put("fork")
‖ space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork")

⇓

board |-> "fork" |-                                                                  board |-> nil |- 
  space.put("fork")                                                          =>      space.put("fork")  
‖ space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork")       ‖ space.get("fork"); space.put("fork"); space.put("fork")

⇓                                                                                    ||
                                                                                     ||
board |-> "fork"*"fork" |-                                                           ||
  space.get("fork"); space.get("fork"); space.put("fork"); space.put("fork")         ||
                                                                                     ||
⇓                                                                                    ||  
                                                                                     ||
board |-> "fork" |-                                                               <==//
  space.get("fork"); space.put("fork"); space.put("fork") 

⇓

board |-> nil |- 
  space.put("fork"); space.put("fork")

⇓

board |-> "fork" |- 
  space.put("fork")

⇓

board |-> "fork"*"fork" |- 
  0


